package com.example;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.util.Assert.isNull;
import static org.springframework.util.Assert.isTrue;

@SpringBootTest
class DemoCiCdApplicationTests {

	@ParameterizedTest
	@NullSource
	void testIfValueIsNull(String value) {
		isNull(value, () -> "value is null");
	}

	@ParameterizedTest
	@ValueSource(booleans = true)
	void checkIfValueIsTrue(boolean value) {
		isTrue(value, () -> "value is true");
	}

}
